package com.ksga.ppbtbdemo.model;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;


/**
 * Student POJO with encapsulation
 * Learn Lombok to reduce boilerplate code
 */
public class Student {

    @NotEmpty(message = "Name cannot be empty.")
    private String name;

    @NotEmpty(message = "Email cannot be empty.")
    @Email
    private String email;

    @NotEmpty(message = "Classroom cannot be empty.")
    @Size(min = 2, max = 3)
    private String classroom;

    private String profile;

    public Student() {
    }

    public Student(String name, String email, String classroom) {
        this.name = name;
        this.email = email;
        this.classroom = classroom;
    }

    public String getProfile() {
        return profile;
    }

    public void setProfile(String profile) {
        this.profile = profile;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getClassroom() {
        return classroom;
    }

    public void setClassroom(String classroom) {
        this.classroom = classroom;
    }

    @Override
    public String toString() {
        return "Student{" +
                "name='" + name + '\'' +
                ", email='" + email + '\'' +
                ", classroom='" + classroom + '\'' +
                '}';
    }
}
