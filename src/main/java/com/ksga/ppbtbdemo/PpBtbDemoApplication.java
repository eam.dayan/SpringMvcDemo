package com.ksga.ppbtbdemo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PpBtbDemoApplication {

    public static void main(String[] args) {
        SpringApplication.run(PpBtbDemoApplication.class, args);
    }

}
