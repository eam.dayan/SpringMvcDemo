package com.ksga.ppbtbdemo.repository;

import com.ksga.ppbtbdemo.model.Student;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

/**
 * Used to store and retrieve data
 */
@Repository
public class StudentRepository {

    private final List<Student> students = new ArrayList<>();

    // select * from students
    public List<Student> getAllStudents(){
        return this.students;
    }

    // insert into students values (?,?,?,?)
    public void insertStudent(Student student){
        students.add(student);
    }
}
