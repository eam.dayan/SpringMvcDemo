package com.ksga.ppbtbdemo.controller;

import com.ksga.ppbtbdemo.model.Student;
import com.ksga.ppbtbdemo.repository.StudentRepository;
import com.ksga.ppbtbdemo.service.FileStorageService;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import javax.validation.Valid;
import java.io.IOException;

/**
 * Used to handle HTTP requests and responses
 */
@Controller
public class StudentController {

    private final StudentRepository studentRepository;
    private final FileStorageService fileStorageService;


    StudentController(
            StudentRepository studentRepository,
            @Qualifier("fileStorageServiceImp") FileStorageService fileStorageService
    ) {
        this.fileStorageService = fileStorageService;
        this.studentRepository = studentRepository;

        studentRepository.insertStudent(new Student("kimlang", "kimlang@gmail.com", "PP"));
        studentRepository.insertStudent(new Student("krisna", "krisna@gmail.com", "BTB"));
        studentRepository.insertStudent(new Student("sal", "sal@gmail.com", "BTB"));
    }

    @GetMapping
    public ModelAndView getHomePage() {

        System.out.println(fileStorageService.saveFileVersion2());

        ModelAndView mv = new ModelAndView();
        mv.setViewName("index");
        mv.addObject("welcomeMessage", "Welcome to Student Management");
        mv.addObject("students", studentRepository.getAllStudents());
        return mv;
    }


    @GetMapping("/student-registration")
    public ModelAndView getRegistrationPage() {

        Student student = new Student();

        ModelAndView mv = new ModelAndView();
        mv.setViewName("pages/student-registration");
        mv.addObject("newStudent", student);
        return mv;
    }

    @PostMapping("/insert")
    public ModelAndView insertNewStudent(
            @RequestParam("file") MultipartFile file,
            @Valid @ModelAttribute("newStudent") Student student,
            BindingResult bindingResult
    ) {

        if (bindingResult.hasErrors()) {
            ModelAndView mv = new ModelAndView();
            System.out.println(" Code here it works : ");
            mv.setViewName("pages/student-registration");
            return mv;
        }

        // try to save file
        try {
            String profileUrl = "http://localhost:8080/images/" + fileStorageService.saveFile(file);
            System.out.println("File URL: " + profileUrl);

            student.setProfile(profileUrl);
        } catch (IOException ex) {

            System.out.println("File Exception : " + ex.getMessage());
        }

        studentRepository.insertStudent(student);
        ModelAndView mv = new ModelAndView();
        mv.setViewName("redirect:/");
        return mv;
    }


//    @GetMapping
//    public String getHomePageUsingShortcut(Model model){
//        model.addAttribute("asd", students);
//        return "demo";
//    }

}
