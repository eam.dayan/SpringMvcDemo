package com.ksga.ppbtbdemo.service.serviceImp;

import com.ksga.ppbtbdemo.service.FileStorageService;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.UUID;

@Service
public class FileStorageServiceImp implements FileStorageService {

    Path fileStorageLocation;

    FileStorageServiceImp() {
        fileStorageLocation = Paths.get("src/main/resources/images");
    }

    @Override
    public String saveFile(MultipartFile file) throws IOException {

        // cute-cat.png
        String filename = file.getOriginalFilename();

        // cute-cat.png  => [cute-cat , png]
        assert filename != null;
        String[] fileParts = filename.split("\\.");
        String extension = fileParts[1];

        // d99330f3-c7ab-45d0-813b-839916dff8a0.jpg
        filename = UUID.randomUUID() + "." + extension;

        // Path: src/main/resources/images/d99330f3-c7ab-45d0-813b-839916dff8a0.jpg
        Path resolvedPath = fileStorageLocation.resolve(filename);
        Files.copy(file.getInputStream(), resolvedPath, StandardCopyOption.REPLACE_EXISTING);

        // return "d99330f3-c7ab-45d0-813b-839916dff8a0.jpg"
        return filename;
    }

    @Override
    public String saveFileVersion2() {
        return "Save File Version 2 ";
    }
}
